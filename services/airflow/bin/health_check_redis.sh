#!/bin/bash
# https://github.com/docker-library/healthcheck/blob/master/redis/docker-healthcheck

set -eo pipefail

host="$(hostname -i || echo '127.0.0.1')"

if ping="$(redis-cli -h "$host" ping)" && [ "$ping" = 'PONG' ]; then
	exit 1
fi

exit 1

BAP: EKS + Airflow + MLflow
===========================

*NOTE: All credentials in this document have been deactivated.*

#### Create ECR Repository

Create two ECR repositories for airflow and mlflow docker images using the AWS console.

- bap-airflow
- bap-mlflow


#### Build & push docker images

    $ cd services/airflow    
    $ docker build -t 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-airflow:0.1 .
    $ docker push 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-airflow:0.1
    
    $ cd services/mlflow
    $ docker build -t 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-mlflow:0.1 .
    $ docker push 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-mlflow:0.1
    

#### Install terraform    
    
Install terraform from [here](https://www.terraform.io/downloads.html).

    $ terraform --version 
    Terraform v0.11.13    


#### Install helm client

Install helm from [here](https://helm.sh/docs/using_helm/#installing-helm).

    $ helm version
    Client: &version.Version{SemVer:"v2.13.1", GitCommit:"618447cbf203d147601b4b9bd7f8c37a5d39fbb4", GitTreeState:"clean"}

---

## Terminal sessions

- [Provision EKS Cluster](https://asciinema.org/a/bfejR8nmGy3ivGzaoZhy15q7N)
- [Setup EKS auth](https://asciinema.org/a/UoOGlGLgyACFkBpbb1TpYra1N)
- [Install helm server](https://asciinema.org/a/Qdoc3VmMTSKCVGwwW4FTLeiFH)
- [Install BAP core dependencies](https://asciinema.org/a/kVCF0mwaieusiP8RTcNUntZi3)
- [Install airflow](https://asciinema.org/a/ByUr0qztGQ5w3fEliwhGTuM1N)

---

#### Provision EKS Cluster 

*Architecture*

![image info](docs/img/eks.svg)


Configure the AWS provider depending on your local setup in `providers.tf`.

Initialize terraform.


    $ cd infra/tf-modules/eks
    $ terraform init

Create a `yum.tfvars` file.


    $ cat `yum.tfvars`
    case_code = "yum"
    
    tier = "Development"
    
    tags = {
      Version = "0.1"
    }
    
    vpc_id = "vpc-0975360133b04e6eb"
    
    subnet_ids = [
      "subnet-024cd03efd7996530",
      "subnet-073b72d3816a76672"
    ]
    
    ssh_source_cidr_blocks = [
      "10.0.0.0/8"
    ]
    
    instance_type = "t3.medium"
    
    ami_id = "ami-0abcb9f9190e867ab"
    
    keypair_name = "yum"
    
    aws_profile = "aagbap"
    
    asg_min_size = 1
    
    asg_max_size = 5
    
    asg_desired_capacity = 3


Create a ssh keypair via the AWS console (AWS -> EC2 -> Network & Security -> Key Pairs).
This wil be used by the auto-scaling group for the kubernetes nodes. Update the `keypair_name` in `yum.tfvars`.

Change any values specific to your set-up. The important ones being `vpc_id`, `subnet_id`, and `ami_id`. 
Please see docs in the `variables.tf` file for information on each variable.  

Apply terraform module. 


    $ terraform apply --var-file yum.tfvars
    eks_node_role_arn = arn:aws:iam::059654891895:role/ec2-bap-yum-node
    kube_config = ~/.kube/config-yum
    kube_context = yum    
    

#### Setup EKS Auth 

Configure the AWS provider depending on your local setup in `providers.tf`.

Initialize terraform.


    $ terraform init

Create a `yum.tfvars` file.


    $ cat yum.tfvars
    kube_config = "~/.kube/config-yum"
    
    kube_context = "yum"
    
    eks_node_role_arn = "arn:aws:iam::059654891895:role/ec2-bap-yum-node"
    
    eks_admin_role_arns = [
      "arn:aws:iam::059654891895:role/AAG_Admins",
      "arn:aws:iam::182581879643:role/AAG_Admins"
    ]

Change values depending on your AWS environment.


Apply terraform module. 


    $ terraform plan --var-file yum.tfvars
    $ terraform apply --var-file yum.tfvars
    aws-auth-map-roles = 
    - rolearn: arn:aws:iam::059654891895:role/ec2-bap-yum-node
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
    - rolearn: arn:aws:iam::059654891895:role/AAG_Admins
      username: admin-0
      groups:
        - system:masters
    - rolearn: arn:aws:iam::182581879643:role/AAG_Admins
      username: admin-1
      groups:
        - system:masters    

Verify that the kubernetes nodes have joined the cluster.

    
    $ kubectl get nodes
    NAME                           STATUS    ROLES     AGE       VERSION
    ip-10-91-32-103.ec2.internal   Ready     <none>    47m       v1.12.7
    ip-10-91-32-52.ec2.internal    Ready     <none>    47m       v1.12.7
    ip-10-91-33-23.ec2.internal    Ready     <none>    47m       v1.12.7


#### Install helm server

Configure the AWS provider depending on your local setup in `providers.tf`.

Initialize terraform.


    $ cd infra/tf-modules/k8s-helm
    $ terraform init

Create a `yum.tfvars` file.


    $ cat yum.tfvars
    kube_config = "~/.kube/config-yum"

    kube_context = "yum"
    
    
Apply terraform module. 


    $ terraform plan --var-file yum.tfvars
    $ terraform apply --var-file yum.tfvars    


Verify that tiller is installed.


    $ helm version
    Client: &version.Version{SemVer:"v2.12.3", GitCommit:"eecf22f77df5f65c823aacd2dbd30ae6c65f186e", GitTreeState:"clean"}
    Server: &version.Version{SemVer:"v2.12.3", GitCommit:"eecf22f77df5f65c823aacd2dbd30ae6c65f186e", GitTreeState:"clean"} 

---

Provision BAP core dependencies
-------------------------------

This terraform module installs core resources required by Airflow and MLFlow.

Configure the AWS provider depending on your local setup in `providers.tf`.

Initialize terraform.


    $ cd infra/tf-modules/bap-core
    $ terraform init
 

Create a `yum.tfvars` file.


    $ cat yum.tfvars
    case_code = "yum"
    
    tier = "Development"
    
    tags = {
      Version = "0.1"
    }
    
    subnet_ids = [
      "subnet-024cd03efd7996530",
      "subnet-073b72d3816a76672",
    ]
    
    vpc_security_group_ids = [
      "sg-0117000725816f98b",
    ]
    
    rds_instance_class = "db.t3.medium"
    
    redis_node_type = "cache.m3.medium"


Set the proper values for `subnet_ids` and `vpc_security_group_ids`, and others if desired.

Apply terraform module. 


    $ terraform plan --var-file yum.tfvars
    $ terraform apply --var-file yum.tfvars
    Apply complete! Resources: 12 added, 0 changed, 0 destroyed.

    Outputs:
    
    airflow_pg_host = bap-yum-airflow.cowuyh9rs4gx.us-east-1.rds.amazonaws.com
    airflow_pg_password = KV3C8hVptjBfbTcQ
    airflow_pg_user = airflow
    airflow_redis_host = bap-yum.nigrs6.ng.0001.use1.cache.amazonaws.com
    mlflow_pg_host = bap-yum-mlflow.cowuyh9rs4gx.us-east-1.rds.amazonaws.com
    mlflow_pg_password = DCWHTQd1y63LgA1T
    mlflow_pg_user = mlflow
    mlflow_s3_bucket = bap-yum-mlflow-artifact-store-xnzv
    mlflow_s3_artifact_store_access_key_id = AKIAQ3Y5VNF33DDYFGH4
    mlflow_s3_artifact_store_secret_access_key = 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
    
    
RDS and Redis instance are ready.
    
---

Install airflow
---------------

Create a helm value override file with database and redis values based on the previous step. Also, set the proper values in the `image` block.


    $ export KUBECONFIG=~/.kube/config-yum
    $ cat yum.values.yaml
    image:
      repository: 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-airflow
      tag: 0.3
    
    secrets:
      AIRFLOW__CORE__SQL_ALCHEMY_CONN: "postgresql+psycopg2://airflow:KV3C8hVptjBfbTcQ@bap-yum-airflow.cowuyh9rs4gx.us-east-1.rds.amazonaws.com/airflow"
      AIRFLOW__CORE__EXECUTOR: "CeleryExecutor"
      AIRFLOW__CORE__FERNET_KEY: "nG60e2CHctNIs90Y5b7o9s86Bh6lwLHHkJnJFaZqrCA="
      AIRFLOW__CELERY__BROKER_URL: "redis://bap-yum.nigrs6.ng.0001.use1.cache.amazonaws.com:6379/1"
      AIRFLOW__CELERY__RESULT_BACKEND: "db+postgresql://airflow:KV3C8hVptjBfbTcQ@bap-yum-airflow.cowuyh9rs4gx.us-east-1.rds.amazonaws.com/airflow"
    
    workers:
      replicaCount: 3
  

Install


    $ helm install --name airflow -f yum.values.yaml --wait .
    
Verify


    $ kubectl get pods
    NAME                                 READY     STATUS    RESTARTS   AGE
    airflow-scheduler-69cfbbd468-dbnj9   1/1       Running   0          4m
    airflow-webserver-85cdbd5b96-444bl   1/1       Running   0          4m
    airflow-worker-0                     1/1       Running   0          4m
    airflow-worker-1                     1/1       Running   0          4m
    airflow-worker-2                     1/1       Running   0          4m


Use Airflow
-----------

Install [stern](https://github.com/wercker/stern). Stern is a log tailing utility with some advanced features compared to the native `logs` command.

#### Access airflow webserver

    ./bin/airflow-webserver-port-forward.sh
    # Open http://127.0.0.1:8080
    
    
#### Watch logs

- Webserver: `./bin/airflow-webserver-watch-logs.sh`
- Scheduler: `./bin/airflow-scheduler-watch-logs.sh`
- Workers: `./bin/airflow-workers-watch-logs.sh`  


Install MLflow
--------------

Create a helm value override file with database and redis values based on the previous step. Also, set the proper values in the `image` block.


    $ cd infra/helm-charts/mlflow
    $ export KUBECONFIG=~/.kube/config-yum
    $ cat yum.values.yaml
    image:
      repository: 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-mlflow
      tag: 0.2
    
    secrets:
      BACKEND_STORE_URI: "postgresql://mlflow:DCWHTQd1y63LgA1T@bap-yum-mlflow.cowuyh9rs4gx.us-east-1.rds.amazonaws.com:5432/mlflow"
      DEFAULT_ARTIFACT_ROOT: "s3://bap-yum-mlflow-artifact-store-xnzv/"
      AWS_ACCESS_KEY_ID: "AKIAQ3Y5VNF33DDYFGH4"
      AWS_SECRET_ACCESS_KEY: "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"


Install


    $ helm install --name mlflow -f yum.values.yaml --wait .
    
Verify


    $ kubectl get pods
    
    
Use MLflow
----------

#### Access Tracking UI

    ./bin/mlflow-tracking-ui-port-forward.sh
    # Open http://127.0.0.1:5000
    
![image info](docs/img/mlflow-without-runs.png)    

#### Log a ML model

Configure AWS credentials in `docker-compose.yaml`.

    $ cd services/mlflow
    $ cat docker-compose.yaml
    version: "3.7"
    
    services:
    
      mlflow:
        container_name: mlflow
        build: .
        volumes:
          - .:/opt/mlflow
        entrypoint: ["tail", "-f", "/dev/null"]
        environment:
          MLFLOW_TRACKING_URI: "http://docker.for.mac.localhost:5000"
          MLFLOW_EXPERIMENT_ID: "0"
          AWS_ACCESS_KEY_ID: "AKIAQ3Y5VNF33DDYFGH4"
          AWS_SECRET_ACCESS_KEY: "CB5lk0N1YKu1t0exeyM+kxdQsMQMd8q884rFuNlT"

    
Replace `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` with the values from the `Provision BAP core dependencies` step.

    $ docker-compose up
    > Starting mlflow ... done
    > Attaching to mlflow 

    $ docker exec -ti mlflow sh
    docker> cd /opt/mlflow/examples
    docker> python quickstart/mlflow_tracking.py
    docker> python sklearn_logistic_regression/train.py

![image info](docs/img/mlflow-with-runs.png)

Continuous Deployment
---------------------

See `bin/cd.sh`.

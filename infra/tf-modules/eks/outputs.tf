output "kube_config" {
  depends_on = [
    "${local_file.kube_config}",
  ]

  value       = "~/.kube/config-${var.case_code}"
  description = "Location of the kube config file."
}

output "kube_context" {
  value       = "${var.case_code}"
  description = "Kube context."
}

output "eks_node_role_arn" {
  value       = "${aws_iam_role.node.arn}"
  description = "EKS node role ARN."
}

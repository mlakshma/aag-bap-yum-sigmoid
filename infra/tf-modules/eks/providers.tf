provider "aws" {
  version = "2.6.0"
  region  = "us-east-1"
  profile = "aag-bap"
   assume_role {
    role_arn = "arn:aws:iam::621574031683:role/TerraformRole"
    }
}

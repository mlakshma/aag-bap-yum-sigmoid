provider "random" {
  version = "2.1.1"
}

provider "local" {
  version = "1.2.1"
}

data "aws_iam_policy_document" "eks" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "master" {
  name               = "ec2-bap-${var.case_code}-eks-master"
  description        = "BAP: EKS Master Role"
  assume_role_policy = "${data.aws_iam_policy_document.eks.json}"

  tags = "${var.tags}"
}

data "aws_iam_policy" "AmazonEKSClusterPolicy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

data "aws_iam_policy" "AmazonEKSServicePolicy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
}

resource "aws_iam_role_policy_attachment" "attach_AmazonEKSClusterPolicy" {
  role       = "${aws_iam_role.master.name}"
  policy_arn = "${data.aws_iam_policy.AmazonEKSClusterPolicy.arn}"
}

resource "aws_iam_role_policy_attachment" "attach_AmazonEKSServicePolicy" {
  role       = "${aws_iam_role.master.name}"
  policy_arn = "${data.aws_iam_policy.AmazonEKSServicePolicy.arn}"
}

resource "aws_security_group" "master" {
  name        = "bap-${var.case_code}-eks-master-sg"
  description = "BAP: EKS Master security group"
  vpc_id      = "${var.vpc_id}"

  tags = "${merge(
    map("Name", var.case_code),
    var.tags
  )}"
}

resource "aws_eks_cluster" "this" {
  name     = "bap-${var.case_code}"
  role_arn = "${aws_iam_role.master.arn}"

  vpc_config {
    security_group_ids = ["${aws_security_group.master.id}"]
    subnet_ids         = "${var.subnet_ids}"
  }

  depends_on = [
    "aws_iam_role_policy_attachment.attach_AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.attach_AmazonEKSServicePolicy",
  ]
}

locals {
  kube_config = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    server: ${aws_eks_cluster.this.endpoint}
    certificate-authority-data: ${aws_eks_cluster.this.certificate_authority.0.data}
  name: ${var.case_code}
contexts:
- context:
    cluster: ${var.case_code}
    user: ${var.case_code}
  name: ${var.case_code}
current-context: ${var.case_code}
kind: Config
preferences: {}
users:
- name: ${var.case_code}
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${aws_eks_cluster.this.name}"
      env:
      - name: AWS_PROFILE
        value: ${var.aws_profile}
KUBECONFIG
}

resource "local_file" "kube_config" {
  content  = "${local.kube_config}"
  filename = "/tmp/config-${var.case_code}"

  provisioner "local-exec" {
    command = "cp /tmp/config-${var.case_code} ~/.kube/config-${var.case_code}"
  }

  depends_on = [
    "aws_eks_cluster.this",
  ]
}

data "aws_iam_policy_document" "ec2" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "node" {
  name               = "ec2-bap-${var.case_code}-node"
  description        = "BAP: EKS Node Role"
  assume_role_policy = "${data.aws_iam_policy_document.ec2.json}"

  tags = "${var.tags}"
}

data "aws_iam_policy" "AmazonEKSWorkerNodePolicy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

data "aws_iam_policy" "AmazonEKS_CNI_Policy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}

data "aws_iam_policy" "AmazonEC2ContainerRegistryReadOnly" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "attach_AmazonEKSWorkerNodePolicy" {
  role       = "${aws_iam_role.node.name}"
  policy_arn = "${data.aws_iam_policy.AmazonEKSWorkerNodePolicy.arn}"
}

resource "aws_iam_role_policy_attachment" "attach_AmazonEKS_CNI_Policy" {
  role       = "${aws_iam_role.node.name}"
  policy_arn = "${data.aws_iam_policy.AmazonEKS_CNI_Policy.arn}"
}

resource "aws_iam_role_policy_attachment" "attach_AmazonEC2ContainerRegistryReadOnly" {
  role       = "${aws_iam_role.node.name}"
  policy_arn = "${data.aws_iam_policy.AmazonEC2ContainerRegistryReadOnly.arn}"
}

resource "aws_iam_instance_profile" "node" {
  name = "ec2-bap-${var.case_code}-node"
  role = "${aws_iam_role.node.name}"
}

resource "aws_security_group" "node" {
  name        = "bap-${var.case_code}-eks-node-sg"
  description = "BAP: EKS Node security group"
  vpc_id      = "${var.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(
    map("Name", var.case_code),
    var.tags
  )}"
}

resource "aws_security_group_rule" "node_ingress_self" {
  description              = "Allow nodes to communicate with each other"
  protocol                 = "-1"
  from_port                = 0
  to_port                  = 65535
  security_group_id        = "${aws_security_group.node.id}"
  source_security_group_id = "${aws_security_group.node.id}"
  type                     = "ingress"
}

resource "aws_security_group_rule" "node_ingress_cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  protocol                 = "tcp"
  from_port                = 1025
  to_port                  = 65535
  security_group_id        = "${aws_security_group.node.id}"
  source_security_group_id = "${aws_security_group.master.id}"
  type                     = "ingress"
}

resource "aws_security_group_rule" "node_ingress_cluster_https" {
  description              = "Allow pods running extension API servers on port 443 to receive communication from cluster control plane."
  protocol                 = "tcp"
  from_port                = 443
  to_port                  = 443
  security_group_id        = "${aws_security_group.node.id}"
  source_security_group_id = "${aws_security_group.master.id}"
  type                     = "ingress"
}

resource "aws_security_group_rule" "cluster_ingress_node_https" {
  description              = "Allow pods to communicate with the cluster API Server"
  protocol                 = "tcp"
  from_port                = 443
  to_port                  = 443
  security_group_id        = "${aws_security_group.master.id}"
  source_security_group_id = "${aws_security_group.node.id}"
  type                     = "ingress"
}

resource "aws_security_group_rule" "cluster_egress_node" {
  description              = "Allow the cluster control plane to communicate with worker Kubelets and pods."
  protocol                 = "tcp"
  from_port                = 1025
  to_port                  = 65535
  security_group_id        = "${aws_security_group.master.id}"
  source_security_group_id = "${aws_security_group.node.id}"
  type                     = "egress"
}

resource "aws_security_group_rule" "cluster_egress_node_https" {
  description              = "Allow the cluster control plane to communicate with pods running extension API servers on port 443."
  protocol                 = "tcp"
  from_port                = 443
  to_port                  = 443
  security_group_id        = "${aws_security_group.master.id}"
  source_security_group_id = "${aws_security_group.node.id}"
  type                     = "egress"
}

resource "aws_security_group_rule" "node_ingress_ssh" {
  description       = "Allow ssh access to the nodes."
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  security_group_id = "${aws_security_group.node.id}"
  cidr_blocks       = "${var.ssh_source_cidr_blocks}"
  type              = "ingress"
}

locals {
  node_userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh ${aws_eks_cluster.this.name}
USERDATA
}

resource "aws_launch_configuration" "this" {
  name                        = "bap-${var.case_code}"
  associate_public_ip_address = false
  iam_instance_profile        = "${aws_iam_instance_profile.node.name}"
  image_id                    = "${var.ami_id}"
  instance_type               = "${var.instance_type}"
  user_data_base64            = "${base64encode(local.node_userdata)}"
  key_name                    = "${var.keypair_name}"

  security_groups = [
    "${aws_security_group.node.id}",
  ]

  root_block_device {
    volume_type = "gp2"
    volume_size = 100
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "this" {
  launch_configuration = "${aws_launch_configuration.this.id}"
  min_size             = "${var.asg_min_size}"
  max_size             = "${var.asg_max_size}"
  desired_capacity     = "${var.asg_desired_capacity}"
  name                 = "bap-${var.case_code}"
  vpc_zone_identifier  = "${var.subnet_ids}"

  tag {
    key                 = "Name"
    value               = "bap-${var.case_code}"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${aws_eks_cluster.this.name}"
    value               = "owned"
    propagate_at_launch = true
  }

  tag {
    key                 = "CaseCode"
    value               = "${var.case_code}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Tier"
    value               = "${var.tier}"
    propagate_at_launch = true
  }

  tag {
    key                 = "ManagedBy"
    value               = "Terraform"
    propagate_at_launch = true
  }
}

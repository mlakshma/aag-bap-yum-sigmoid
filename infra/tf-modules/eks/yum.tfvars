case_code = "yum"

tier = "Dev"

tags = {
  Version = "0.1"
}

vpc_id = "vpc-07f1dd030ace59e65"

subnet_ids = [
  "subnet-08ff464672fab0d35",
  "subnet-0cd6796b98e604090"
]

ssh_source_cidr_blocks = [
  "10.18.0.0/16"
]

instance_type = "t3.medium"

ami_id = "ami-0abcb9f9190e867ab"

keypair_name = "yum"

aws_profile = "aag-bap"

asg_min_size = 1

asg_max_size = 5

asg_desired_capacity = 3
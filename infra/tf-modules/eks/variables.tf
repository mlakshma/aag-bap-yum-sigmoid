variable "case_code" {
  description = "Unique BAP case code. It must only contain lower case letters and numbers."
}

variable "tier" {
  default     = "Development"
  description = "Ex. 'Development' or 'Production'."
}

variable "tags" {
  default     = {}
  type        = "map"
  description = "Tags to apply to all AWS resources. Defaults to an empty list."
}

variable "vpc_id" {
  description = "VPC id."
}

variable "subnet_ids" {
  type        = "list"
  description = "List of subnet ids."
}

variable "ssh_source_cidr_blocks" {
  type        = "list"
  description = "CIDR range from which to allow ssh access to the Kubernetes nodes. This is generally bastions."
}

variable "instance_type" {
  default     = "t3.medium"
  description = "Kubernetes node ec2 instance type."
}

variable "ami_id" {
  description = "AMI id. Note that this value is region specific. Please change accordingly."
}

variable "keypair_name" {
  description = "SSH keypair to install on the Kubernetes nodes."
}

variable "aws_profile" {
  description = "AWS profile to use for generating the '~/.kube/config-*' file."
}

variable "asg_min_size" {
  default     = 1
  description = "Auto-scaling min size."
}

variable "asg_max_size" {
  default     = 5
  description = "Auto-scaling max size."
}

variable "asg_desired_capacity" {
  default     = 3
  description = "Auto-scaling desired capacity."
}

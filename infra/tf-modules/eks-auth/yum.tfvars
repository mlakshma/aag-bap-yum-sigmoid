kube_config = "~/.kube/config-yum"

kube_context = "yum"

eks_node_role_arn = "arn:aws:iam::621574031683:role/ec2-bap-yum-node"

eks_admin_role_arns = [
  "arn:aws:iam::621574031683:role/TerraformRole"
]
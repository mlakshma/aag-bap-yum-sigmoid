provider "local" {
  version = "1.1"
}

provider "kubernetes" {
  version = "1.5.0"

  config_path    = "${var.kube_config}"
  config_context = "${var.kube_context}"
}

# TODO(Saimon): Use Kubernetes provider to setup the cluster role and binding when it is supported. https://github.com/terraform-providers/terraform-provider-kubernetes/pull/229

output "aws-auth-map-roles" {
  value       = "${data.template_file.map_roles.rendered}"
  description = "aws-auth configmap's maproles object."
}

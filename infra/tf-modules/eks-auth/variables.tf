variable "kube_config" {
  description = "Path to the kube config file."
}

variable "kube_context" {
  description = "Kube config context."
}

variable "eks_node_role_arn" {
  description = "EKS node role ARN."
}

variable "eks_admin_role_arns" {
  type        = "list"
  description = "List of EKS admin role ARNS."
}

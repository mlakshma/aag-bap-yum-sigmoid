data "template_file" "admin_roles" {
  template = "${file("${path.module}/templates/admin-role.tmpl")}"
  count    = "${length(var.eks_admin_role_arns)}"

  vars {
    arn  = "${var.eks_admin_role_arns[count.index]}"
    name = "admin-${count.index}"
  }
}

data "template_file" "map_roles" {
  template = "${file("${path.module}/templates/map-roles.tmpl")}"

  vars {
    eks_node_role_arn = "${var.eks_node_role_arn}"
    admin_roles       = "${join("\n", data.template_file.admin_roles.*.rendered)}"
  }
}

resource "kubernetes_config_map" "aws_auth" {
  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }

  data {
    mapRoles = "${data.template_file.map_roles.rendered}"
  }
}

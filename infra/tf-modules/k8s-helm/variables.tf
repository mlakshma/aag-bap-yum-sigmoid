variable "kube_config" {
  description = "Path to the kube config file."
}

variable "kube_context" {
  description = "Kube config context."
}

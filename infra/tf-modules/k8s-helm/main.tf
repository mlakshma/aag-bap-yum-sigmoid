resource "kubernetes_service_account" "tiller" {
  metadata {
    name      = "tiller"
    namespace = "kube-system"
  }

  automount_service_account_token = true
}

resource "kubernetes_cluster_role_binding" "tiller" {
  metadata {
    name = "tiller"
  }

  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${kubernetes_service_account.tiller.metadata.0.name}"
    namespace = "${kubernetes_service_account.tiller.metadata.0.namespace}"
    api_group = ""
  }

  provisioner "local-exec" {
    command = "helm init --kubeconfig ${var.kube_config} --kube-context ${var.kube_context} --service-account=${kubernetes_service_account.tiller.metadata.0.name} --wait"
  }

  provisioner "local-exec" {
    when = "destroy"

    # TODO(Saimon): Remove replicasets deletion command after this fix is released. https://github.com/helm/helm/pull/5161
    command = "helm reset --kubeconfig ${var.kube_config} --kube-context ${var.kube_context} --force --tiller-connection-timeout 10; kubectl delete replicasets -l app=helm,name=tiller -n kube-system --kubeconfig ${var.kube_config}"
  }

  depends_on = [
    "kubernetes_service_account.tiller",
  ]
}

provider "kubernetes" {
  version = "1.6.0"

  config_path    = "${var.kube_config}"
  config_context = "${var.kube_context}"
}

# TODO(Saimon): This provider doesn't remove tiller during destroy. When this feature is added, switch to this provider.
//provider "helm" {
//  version = "0.7.0"
//
//  kubernetes {
//    config_path    = "${var.kube_config}"
//    config_context = "${var.kube_context}"
//  }
//
//  install_tiller  = true
//  service_account = "${kubernetes_service_account.tiller.metadata.0.name}"
//  namespace       = "${kubernetes_service_account.tiller.metadata.0.namespace}"
//  tiller_image    = "gcr.io/kubernetes-helm/tiller:v2.12.3"
//}


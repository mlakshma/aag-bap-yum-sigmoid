case_code = "yum"

tier = "Dev"

tags = {
  Version = "0.1"
}

subnet_ids = [
  "subnet-08ff464672fab0d35",
  "subnet-0cd6796b98e604090"
]

vpc_security_group_ids = [
  "sg-08b5649853cc591d7"
]

rds_instance_class = "db.t3.medium"

redis_node_type = "cache.m3.medium"

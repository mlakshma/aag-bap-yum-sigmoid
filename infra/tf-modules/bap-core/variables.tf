variable "case_code" {
  description = "Unique BAP case code. It must only contain lower case letters and numbers."
}

variable "tier" {
  default     = "Development"
  description = "Ex. 'Development' or 'Production'."
}

variable "tags" {
  default     = {}
  type        = "map"
  description = "Tags to apply to all AWS resources. Defaults to an empty list."
}

variable "subnet_ids" {
  type        = "list"
  description = "List of subnet ids."
}

variable "vpc_security_group_ids" {
  type        = "list"
  description = "List of VPC security group ids. This is used by ElasticCache and RDS."
}

variable "rds_instance_class" {
  default     = "db.t3.medium"
  description = "RDS instance class."
}

variable "redis_node_type" {
  default = "cache.m3.medium"
  description = "Redis node type."
}

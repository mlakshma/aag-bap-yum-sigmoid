output "airflow_pg_host" {
  value       = "${aws_db_instance.airflow_db.address}"
  description = "Postgres host."
}

output "airflow_pg_user" {
  value       = "${aws_db_instance.airflow_db.username}"
  description = "Postgres user."
}

output "airflow_pg_password" {
  value       = "${aws_db_instance.airflow_db.password}"
  description = "Postgres password."
}

output "airflow_redis_host" {
  value       = "${aws_elasticache_replication_group.redis.primary_endpoint_address}"
  description = "Redis host."
}

output "mlflow_pg_host" {
  value       = "${aws_db_instance.mlflow_db.address}"
  description = "Postgres host."
}

output "mlflow_pg_user" {
  value       = "${aws_db_instance.mlflow_db.username}"
  description = "Postgres user."
}

output "mlflow_pg_password" {
  value       = "${aws_db_instance.mlflow_db.password}"
  description = "Postgres password."
}

output "mlflow_s3_bucket" {
  value = "${aws_s3_bucket.mlflow_artifact_store.id}"
  description = "MLFlow: Artifact store s3 bucket."
}

output "mlflow_s3_artifact_store_access_key_id" {
  value = "${aws_iam_access_key.this.id}"
  description = "MLFlow: S3 artifact store AWS access key id."
}

output "mlflow_s3_artifact_store_secret_access_key" {
  value = "${aws_iam_access_key.this.secret}"
  description = "MLFlow: S3 artifact store AWS secret access key."
}


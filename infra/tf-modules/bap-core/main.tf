resource "aws_db_subnet_group" "db_subnet_group" {
  name        = "bap-${var.case_code}"
  description = "BAP: ${var.case_code}"
  subnet_ids  = "${var.subnet_ids}"

  tags = "${var.tags}"
}

resource "random_string" "airflow_db_password" {
  length  = 16
  special = false
}

resource "aws_db_instance" "airflow_db" {
  identifier                = "bap-${var.case_code}-airflow"
  name                      = "airflow"
  username                  = "airflow"
  password                  = "${random_string.airflow_db_password.result}"
  engine                    = "postgres"
  engine_version            = "10.4"
  instance_class            = "${var.rds_instance_class}"
  storage_type              = "gp2"
  allocated_storage         = 50
  db_subnet_group_name      = "${aws_db_subnet_group.db_subnet_group.name}"
  vpc_security_group_ids    = "${var.vpc_security_group_ids}"
  copy_tags_to_snapshot     = true
  final_snapshot_identifier = "bap-${var.case_code}-airflow-backup"

  tags = "${var.tags}"
}

resource "random_string" "mlflow_db_password" {
  length  = 16
  special = false
}

resource "aws_db_instance" "mlflow_db" {
  identifier                = "bap-${var.case_code}-mlflow"
  name                      = "mlflow"
  username                  = "mlflow"
  password                  = "${random_string.mlflow_db_password.result}"
  engine                    = "postgres"
  engine_version            = "10.4"
  instance_class            = "${var.rds_instance_class}"
  storage_type              = "gp2"
  allocated_storage         = 50
  db_subnet_group_name      = "${aws_db_subnet_group.db_subnet_group.name}"
  vpc_security_group_ids    = "${var.vpc_security_group_ids}"
  copy_tags_to_snapshot     = true
  final_snapshot_identifier = "bap-${var.case_code}-mlflow-backup"

  tags = "${var.tags}"
}

resource "random_string" "unique_id" {
  length  = 4
  special = false
  upper   = false
}

resource "aws_s3_bucket" "mlflow_artifact_store" {
  bucket = "bap-${var.case_code}-mlflow-artifact-store-${random_string.unique_id.result}"
  tags   = "${var.tags}"
}

resource "aws_iam_user" "this" {
  name = "bap-${var.case_code}"

  force_destroy = true

  tags = "${var.tags}"
}

resource "aws_iam_user_policy" "s3_policy" {
  name = "mlflow-artifact-store-s3-policy"
  user = "${aws_iam_user.this.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::${aws_s3_bucket.mlflow_artifact_store.id}",
        "arn:aws:s3:::${aws_s3_bucket.mlflow_artifact_store.id}/*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_access_key" "this" {
  user = "${aws_iam_user.this.name}"
}

resource "aws_elasticache_subnet_group" "private" {
  name        = "bap-${var.case_code}"
  description = "BAP"
  subnet_ids  = "${var.subnet_ids}"
}

resource "aws_elasticache_replication_group" "redis" {
  replication_group_id          = "bap-${var.case_code}"
  replication_group_description = "BAP"
  node_type                     = "${var.redis_node_type}"
  number_cache_clusters         = 1
  parameter_group_name          = "default.redis5.0"
  subnet_group_name             = "${aws_elasticache_subnet_group.private.name}"
  security_group_ids            = "${var.vpc_security_group_ids}"
  at_rest_encryption_enabled    = true
  transit_encryption_enabled    = false

  tags = "${var.tags}"
}

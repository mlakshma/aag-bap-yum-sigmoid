#### Create terraform policy

- Goto [https://console.aws.amazon.com/iam/home?#/policies](https://console.aws.amazon.com/iam/home?#/policies)
- Click "Policies".
- Click "Create policy".
- Select "JSON" tab.
- Paste the contents of `TerraformPolicy.json`.
- Click "Review Policy".
- Set name to "TerraformPolicy"
- Click "Create Policy".


#### Create terraform role

- Goto [https://console.aws.amazon.com/iam/home?#/roles](https://console.aws.amazon.com/iam/home?#/roles)
- Click "Create Role'.
- Select second option i.e. "Another AWS account (Belonging to you or 3rd party)".
- Set Account ID to `928178381432` **Sriram, this is the AWS account that you created yesterday for testing. We'll replace this value with Sigmoid's account id.**
- Click "Next: Permissions"
- Click "Filter Policies". Select "Customer Managed"
- Select `TerraformPolicy`
- Click "Next: Tags"
- Click "Next: Review"
- Set Role name to `TerraformRole`
- Click "Create Role"


#### Send role ARN to 3rd party

- Goto [https://console.aws.amazon.com/iam/home?#/roles/TerraformRole](https://console.aws.amazon.com/iam/home?#/roles/TerraformRole)
- Copy the link at "Give this link to users who can switch roles in the console"
- Send the link out.

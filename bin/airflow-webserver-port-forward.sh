#!/usr/bin/env bash

kubectl port-forward $(kubectl get pods -l "app=airflow,component=webserver" -o jsonpath="{.items[0].metadata.name}") 8888:8080

#!/usr/bin/env bash

kubectl port-forward $(kubectl get pods -l "app.kubernetes.io/name=mlflow,component=tracking-ui" -o jsonpath="{.items[0].metadata.name}") 5000

#!/usr/bin/env bash

stern --selector "app=airflow,component=webserver" --tail 25

# kubectl logs po/$(kubectl get pods -l "app=airflow,component=webserver" -o jsonpath="{.items[0].metadata.name}") --tail=25 -f

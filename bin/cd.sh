#!/usr/bin/env bash

git_commit_sha1="0edfd414c0ea0c7e8ff93433673ddf810e00210b"

echo_title () {
    printf "\033[1;34m <<< $1 >>> \033[0m\n"
}

echo_cmd () {
    printf "\033[1;32m$1\033[0m\n"
}

echo_title "Notify: Send email/slack message"
read

echo_title "Clone source repository"
echo_cmd "EXAMPLE: git clone https://github.com/..."
read

echo_title "Build docker image"
echo_cmd "docker build -t 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-airflow:${git_commit_sha1} ."
read
#docker build -t 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-airflow:$git_commit_sha1 ../services/airflow

echo_title "Run unit tests"
echo_cmd "EXAMPLE: docker run 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-airflow:$git_commit_sha1 make unit-tests"
read

echo_title "Run integration tests"
echo_cmd "EXAMPLE: docker-compose up -d"
echo_cmd "EXAMPLE: docker exec airflow make integration-tests"
echo_cmd "EXAMPLE: docker-compose stop"
read

echo_title "Push image to ECR"
echo_cmd "docker push 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-airflow:$git_commit_sha1"
read
#docker push 059654891895.dkr.ecr.us-east-1.amazonaws.com/bap-airflow:$git_commit_sha1

echo_title "Helm: Upgrade deployment"
echo_cmd "helm upgrade airflow -f ../infra/helm-charts/airflow/yum.values.yaml --set image.tag=$git_commit_sha1 --recreate-pods --wait ../helm-charts/airflow"
read
#helm upgrade airflow -f ../helm-charts/airflow/yum.values.yaml --set image.tag=$git_commit_sha1 --recreate-pods --wait ../helm-charts/airflow

echo_title "Notify: Send email/slack message"

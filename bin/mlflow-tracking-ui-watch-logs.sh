#!/usr/bin/env bash

stern --selector "app.kubernetes.io/name=mlflow,component=tracking-ui" --tail 25

# kubectl logs -l "app.kubernetes.io/name=mlflow,component=tracking-ui" --tail=25 -f
